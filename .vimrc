" ==============================================================================
" == General Option ==

set shell=/bin/bash
set history=1000
set suffixes+=.obj,.log,.aux,.ps,.dvi,.class
set tags=.tags,./tags,tags,~/.vim/tags
set path=.,/usr/include,src,inc
let mapleader='\'
" Save vim state before closing
set viminfo='20,\"1000,:100,!
set nonumber
" Program to use with K command
set keywordprg=man
set showtabline=0
set sidescroll=1
" To allow remove of automatically inserted characters
set backspace=indent,eol,start
" set autowriteall
" set sidescrolloff=999
" To open a file where we leave it.
autocmd BufReadPost * if line("'\"") > 0 && line("'\"") <= line("$") |
			\   exe "normal! g`\"" |
			\ endif
" Diff option
set diffopt=filler,vertical
" Make using Ctrl+C do the same as Escape, to trigger autocmd commands
inoremap <C-c> <Esc>


" ==============================================================================
" == UI Options ==

set confirm
set showmode
set showcmd
set cmdheight=1
set laststatus=2
set statusline=%f\ \ %l,%c%V\ %P%=%y%h%m%r\ %<%{FugitiveStatusline()}\ %{strftime('%H:%M:%S\ %d/%m/%Y')}
set ruler
set rulerformat=%20(%l,%c%V\ %P%=%y%h%m%r%)
set wrap
set listchars+=precedes:<,extends:>


" ==============================================================================
" == Terminal Bells ==

set novisualbell
set noerrorbells
set vb t_vb=


" ==============================================================================
" == Search configuration ==

set hlsearch
set matchpairs+=<:>
set matchtime=5
set noignorecase
set showmatch
set incsearch
set hlsearch


" ==============================================================================
" == Backup File ==

set backup
set backupdir=~/.vim/backup


" ==============================================================================
" == File Encoding ==

" set termencoding=ascii
" setglobal fileencoding=ascii
set encoding=utf-8
set termencoding=utf-8
setglobal fileencoding=utf-8
set noendofline
set nobomb


" ==============================================================================
" == Command Completion ==

set wildmode=longest,list
set wildmenu


" ==============================================================================
" == Text Formating ==

" == Load and enable automatic indentation and text highlighning ==
filetype on
filetype plugin indent on

" == Configuration of automatic indentation ==
set textwidth=100
set shiftwidth=8
set tabstop=8
set softtabstop=8
" XXX With ctab.cim, preserveindent has to be set XXX
set preserveindent
set copyindent
set noexpandtab
set smarttab
" set formatoptions=cqn
set formatoptions=roqn1j
" set autoindent
" set smartindent
set cindent
set cinoptions=:0,(0,t0

" == File type custom configuration ==
autocmd BufNewFile,BufRead *.py setlocal expandtab
autocmd BufEnter *.tpp setfiletype cpp
autocmd BufEnter Makefile.def setfiletype make
autocmd BufEnter *.l setfiletype aflex
autocmd BufEnter *.y setfiletype ayacc
autocmd BufEnter CMakeLists.txt setfiletype cmake
autocmd BufEnter *.gvy,*.gy,*.gsh,Jenkinsfile* setfiletype groovy
autocmd BufEnter *.gvy,*.gy,*.gsh,Jenkinsfile* setlocal shiftwidth=4
autocmd BufEnter *.gvy,*.gy,*.gsh,Jenkinsfile* setlocal tabstop=4
autocmd BufEnter *.gvy,*.gy,*.gsh,Jenkinsfile* setlocal softtabstop=4
autocmd BufEnter *.yaml,*.yml setlocal shiftwidth=2
autocmd BufEnter *.yaml,*.yml setlocal tabstop=2
autocmd BufEnter *.yaml,*.yml setlocal softtabstop=2
augroup textformat
	autocmd BufNewFile,BufRead *.txt setfiletype text
	autocmd BufNewFile,BufRead *.txt,*.tex,mut-* setlocal formatlistpat=\\\(^\\\s*\\\d\\\+[\\\]:.)}\\\t\\\ ]\\\s*\\\)\\\|\\\(\\\s*-\\\s\\\+\\\)\\\|\\\(^\\\s*\\\a[\\\]:.)}]\\\s\\\)
augroup END
" autocmd BufEnter *.py,*.c,*.cpp,*.h,*.hpp,*.sh,*.csh setlocal encoding=ascii
" autocmd BufEnter *.py,*.c,*.cpp,*.h,*.hpp,*.sh,*.csh setlocal termencoding=ascii
" autocmd BufEnter *.py,*.c,*.cpp,*.h,*.hpp,*.sh,*.csh setlocal fileencoding=ascii

" autocmd BufEnter * call matchadd('ErrorMsg', '\%>'.&tw.'v.\+', -1)
" autocmd BufEnter *.py,*.c,*.cpp,*.h,*.hpp,*.sh,*.csh call matchadd('Visual', '\%<'.(&tw + 1).'v.\%>'.(&tw - 3).'v', -1)


" ==============================================================================
" == Folding Configuration ==

" XXX DEACTIVATED XXX
" set foldmethod=indent
" set foldnestmax=1
" set foldlevel=0
" set foldcolumn=0
" set foldignore=#


" ==============================================================================
" == Personnal Commands ==

" Generating TAGS commands
function! CtagsGenerate()
	echo "Generating tags..."
	"+ silent !ctags --totals=yes --langmap=C++:.c++.cc.cp.cpp.cxx.h.h++.hh.hp.hpp.hxx.C.H.tpp -R -f "${PWD}/.tags"
	silent !ctags --langmap=C++:.c++.cc.cp.cpp.cxx.h.h++.hh.hp.hpp.hxx.C.H.tpp -R -f "${PWD}/.tags"
	redraw!
	return 0
endfunction
command! -nargs=0 Ctags call CtagsGenerate()

" Generating Cscope data base
function! CscopeMkdb()
	echo "Generating cscope db...\n"
	silent !cscope-mkdb
 	" call inputsave()
	" call input("[INFO] Press enter")
	" call inputrestore()
	redraw!
	return 0
endfunction
command! -nargs=0 CscopeDb call CscopeMkdb()

" Generating DATE
function! DateGenerate()
	echo "generating date..."
	execute "normal i" . strftime("%d/%m/%Y")
endfunction
command! -nargs=0 Date call DateGenerate()


" ==============================================================================
" == Personnal Key Bindings ==

" Pour mettre la fenetre a son maximum
map <M-C-u>    z90<CR>:file<CR>
" Pour mettre la fenetre a son minimum
map <M-C-i>    :wincmd w<CR>z90<CR>:file<CR>
" Pour lancer la generation de tags du makefile courant
" map <M-C-t>    :Ctags<CR>
" Pour Aller sur l'erreur suivante
map <M-C-j>    :cn<CR>
" Pour aller sur l'erreur precedante
map <M-C-k>    :cp<CR>
" Pour lister les erreurs de la compilation
map <M-C-l>    :cl<CR>
" To remove visual mode
map Q A


" ==============================================================================
" == Configuration of ctab plugin ==

let g:ctab_filetype_maps=1
" let g:ctab_disable_checkalign=1
" let g:ctab_enable_default_filetype_maps=1
" let g:ctab_disable_tab_maps=1


" ==============================================================================
" == Configuration of ShowTrailingWhiteSpaces plugin ==


" ==============================================================================
" == Configuration of Man plugin ==
runtime ftplugin/man.vim
let g:ft_man_open_mode = 'vert'
set keywordprg=:Man


" ==============================================================================
" == Configuration of Pathogen ==
execute pathogen#infect()


" ==============================================================================
" == Configuration of Cscope ==
if has("cscope")
	set csprg=/usr/bin/cscope
	set cscopequickfix=s-,c-,d-,i-,t-,e-,a-
	set csto=0
	set cspc=3
	set cst
	set nocsverb
	" add any database in current directory
	if filereadable(".cscope.db")
		cs add .cscope.db
		" else add database pointed to by environment
	endif
	set csverb
endif


" ==============================================================================
" == Configuration of ALE (Asynchronous Lint Engine) ==
let g:ale_enabled = 0
let g:ale_set_quickfix = 0
let g:ale_linters = { 'python': ['pylint'], 'c' : ['cppcheck'], 'sh' : [] }
let g:ale_c_cppcheck_options="--language=c --std=c11 --platform=unix64 --enable=style --inline-suppr"
let g:ale_fix_on_save_ignore = 0


" ==============================================================================
" == Configuration of YouCompleteMe ==
let g:ycm_auto_trigger = 1
let g:ycm_min_num_of_chars_for_completion = 3
let g:ycm_autoclose_preview_window_after_completion = 1
let g:ycm_autoclose_preview_window_after_insertion = 1
let g:ycm_show_diagnostics_ui = 0


" ==============================================================================
" == Configuration of indentLine ==
let g:indentLine_enabled = 0
let g:indentLine_char_list = ['|', '¦', '┆', '┊']
" let g:indentLine_setColors = 1


" ==============================================================================
" == Syntax Higlithning ==

syntax on
set termguicolors

" -- Palenight theme configuration --
" set background=dark
" colorscheme palenight

" -- AYU theme configuration --
" let ayucolor="light"
" let ayucolor="mirage"
let ayucolor="dark"
set background=dark
colorscheme ayu

" == To display indentation and alignment characters ==
set list listchars=eol:$,tab:\|-,space:.,trail:.,extends:>,precedes:<,nbsp:%
set nolist

" == To display long lines ==
" set colorcolumn=+1
function! ShowLongLines()
	let w:match_long_lines = matchadd('ErrorMsg', '\%>'.&tw.'v.\+', -1)
endfunction
autocmd BufEnter *.c,*.cpp,*.h,*.py call ShowLongLines()
command! -nargs=0 ShowLongLines call ShowLongLines()
command! -nargs=0 ClearLongLines call matchdelete(w:match_long_lines)
