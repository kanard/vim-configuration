" Simple script for managing Non Ascii Characters
" Authors: Cedric Koch-Hofer <cedric.koch.hofer@gmail.com>

function! HighlightNonAsciiOff()
    echom "Setting non-ascii highlight off"
    highlight nonascii none
    let g:is_non_ascii_on=0
endfunction

function! HighlightNonAsciiOn()
    " au BufReadPost * syntax match nonascii "[^\x00-\x7F]" containedin=cComment,vimLineComment,pythonComment
    au BufReadPost,BufWritePost * syntax match nonascii "[^\x00-\x7F]"
    echom "Setting non-ascii highlight on"
    highlight nonascii cterm=underline ctermfg=red ctermbg=none term=underline 
    let g:is_non_ascii_on=1
endfunction

function! ToggleHighlightNonAscii()
    if g:is_non_ascii_on == 1
        call HighlightNonAsciiOff()
    else
        call HighlightNonAsciiOn()
    endif
endfunction

silent! call HighlightNonAsciiOn()
command! -nargs=0 ToggleHighlightNonAscii call ToggleHighlightNonAscii()

function! FindNonAscii()
    execute "normal! /[^\\x00-\\x7F]/\<cr>"
    redraw!
endfunction
command! -nargs=0 FindNonAscii call FindNonAscii()

