" Vim syntax file for C++
"
" Maintainer:	cedric koch-hofer <cedric.kochhofer@gmail.com>
" Last change:	ven jun 17 18:54:58 CEST 2005
" Version:     0.1


" add some rules for the problem with the template
" set indentexpr=CppIndent(v:lnum)

" specific command for STL libraries
source ${HOME}/.vim/after/syntax/stl.vim

" specific command for systemc
" source ${HOME}/.vim/after/syntax/systemc.vim

" specific command for ASC
" source ${HOME}/.vim/after/syntax/asc.vim
