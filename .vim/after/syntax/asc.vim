" Vim syntax file for SystemC
"
" Maintainer:	Karthick Gururaj <stonnedsnake@yahoo.com>
" Last change:	Nov 30 2003
" Version:     1.0

let file_syntax = ASC_or_CPP()
if(file_syntax == "cpp")
   finish
endif

" ASC statements
syntax keyword ASC_condition as_choice_nd as_choice_d
syntax keyword ASC_label as_guard

" Macros in ASC
syntax keyword ASC_macro AS_UNTIMED
syntax keyword ASC_macro AS_LOG

" ASC Namespace
syntax match ASC_namespace "\zs\<sc_asc\ze"

" ASC Functions
syntax match ASC_func "\zs\<idle\ze[ |	]*(" 
syntax match ASC_func "\zs\<as_assert\ze[ |	]*(" 
syntax match ASC_func "\zs\<send\ze[ |	]*(" 
syntax match ASC_func "\zs\<receive\ze[ |	]*(" 
syntax match ASC_func "\zs\<par_send\ze[ |	]*(" 
syntax match ASC_func "\zs\<par_receive\ze[ |	]*(" 
syntax match ASC_func "\zs\<probe\ze[ |	]*(" 
syntax match ASC_func "\zs\<as_create_ast_trace_file\ze[ |	]*(" 
syntax match ASC_func "\zs\<as_close_ast_trace_file\ze[ |	]*(" 
syntax match ASC_func "\zs\<as_trace\ze[ |	]*(" 
syntax match ASC_func "\zs\<as_write_comment\ze[ |	]*(" 
syntax match ASC_func "\zs\<as_set_time_unit\ze[ |	]*(" 
syntax match ASC_func "\zs\<as_log\ze" 
syntax match ASC_func "\zs\<as_endl\ze" 

" ASC classes
syntax keyword ASC_type as_active_in as_active_out as_passive_in as_passive_out
syntax keyword ASC_type as_push as_pull
syntax keyword ASC_type as_container as_process
syntax keyword ASC_type as_uint as_int as_bool as_multidigit as_dualrail as_singlerail as_digit
syntax keyword ASC_type as_time as_lagency
syntax keyword ASC_type as_trace_file as_ast_trace_file as_trace_generic
syntax keyword ASC_type as_iterator as_const_iterator as_reverse_iterator as_const_reverse_iterator

" And the highlighting
hi def link ASC_condition Conditional
hi def link ASC_label Label
hi def link ASC_namespace Identifier
hi def link ASC_func Identifier
hi def link ASC_macro Constant
hi def link ASC_type Type

"+ echohl Comment | echo "Detected SystemC file" | echohl None
